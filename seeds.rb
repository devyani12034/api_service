require 'active_record'
require_relative 'models'

puts "---Seeding start---"
puts "---Creating authors---"
(1..100).each do |i|
  name = "user_#{i}"
  user = User.create(username: name, name: name)
  user.create_login
end

ips = ["40.164.80.64", "222.131.199.15", "189.97.184.52", "149.110.198.190", "5.172.203.105", "25.118.126.168", "131.169.66.82", "149.13.211.83", "73.159.204.69", "159.187.140.229", "10.197.247.162", "53.103.112.136", "219.150.182.99", "207.185.158.70", "231.50.16.54", "131.122.133.120", "61.248.21.34", "170.31.116.97", "127.153.110.135", "79.167.176.114", "84.218.229.6", "140.164.59.111", "170.65.117.189", "152.210.246.116", "35.140.90.146", "190.16.7.150", "232.7.98.48", "190.202.134.145", "106.150.187.154", "127.125.174.246", "94.34.242.237", "108.183.245.111", "77.162.12.133", "91.147.93.14", "56.67.166.59", "93.239.66.130", "61.42.96.127", "239.221.181.172", "81.12.147.23", "159.109.30.175", "34.109.171.218", "161.224.138.135", "71.177.102.108", "81.76.240.173", "206.150.180.228", "7.203.72.17", "221.236.191.177", "57.175.242.208", "79.156.121.73", "139.169.47.25"]

puts "---Creating posts---"
User.all.each do |user|
  ips.each do |ip|
    40.times.each do
      user.posts.create(title: Faker::Book.title, content: Faker::Lorem.paragraph, author_ip: ip)
    end
  end
end

puts "---Creating user feedbacks---"
User.all.each do |user|
  user.feedbacks.create(comment: Faker::Lorem.sentence, owner_id: Random.new.rand(1..50))
end

puts "---Creating post feedbacks---"
User.all.each do |user|
  user.posts.first(500).each do |post|
    post.feedbacks.create(comment: Faker::Lorem.sentence, owner_id: Random.new.rand(1..50))
  end
end

puts "---Seeding End---"