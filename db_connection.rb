require 'active_record'
require 'pg'

ActiveRecord::Base.establish_connection(
  { :adapter => 'postgresql',
    :encoding => 'utf8',
    :database => 'api_development',
    :host => '127.0.0.1',
    :port => '5432',
  }
)
