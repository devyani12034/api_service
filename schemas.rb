require 'active_record'

#setting up of user table and columns.
ActiveRecord::Schema.define do
  create_table(:users, force: true, id: false) do |t|
    t.integer 'id', primary_key: true
    t.string 'name'
    t.string 'username'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end
end

ActiveRecord::Schema.define do
  create_table(:logins, force: true, id: false) do |t|
    t.integer 'id', primary_key: true
    t.integer 'user_id', foreign_key: true
    t.string 'auth_token', unique: true
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end
end

ActiveRecord::Schema.define do
  create_table(:posts, force: true, id: false) do |t|
    t.integer 'id', primary_key: true
    t.integer 'user_id', foreign_key: true
    t.string 'title'
    t.text 'content'
    t.string 'author_ip'
    t.decimal 'average_rating', default: 0
    t.integer 'ratings_count', default: 0
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end
end

ActiveRecord::Schema.define do
  create_table(:ratings, force: true, id: false) do |t|
    t.integer 'id', primary_key: true
    t.integer 'post_id', foreign_key: true
    t.integer 'value', default: 0
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end
end

ActiveRecord::Schema.define do
  create_table(:feedbacks, force: true, id: false) do |t|
    t.integer 'id', primary_key: true
    t.references :owner, references: :users, foreign_key: { to_table: :users }
    t.text 'comment'
    t.references :feedbackable, polymorphic: true
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end
end
