require 'active_record'
require 'faker'
require 'sqlite3'
require 'pg'

#This database resides in RAM. This is used for easy execution of the program
#Other databases such as postgresql and mysql could be used also.
# But in that case, a local db has to be there on user's machine

# ActiveRecord::Base.establish_connection(adapter: "sqlite3", database: "dev.sqlite3")

conn = PG.connect(dbname: 'postgres')
conn.exec("DROP DATABASE api_development")
conn.exec("CREATE DATABASE api_development")

require_relative 'db_connection'
require_relative 'models'
require_relative 'schemas'
require_relative 'seeds'