require 'active_record'

class User < ActiveRecord::Base
  has_one :login
  has_many :posts
  has_many :feedbacks, as: :feedbackable
end

class Login < ActiveRecord::Base
  belongs_to :user

  before_create :generate_token

  def generate_token
    self.auth_token = SecureRandom.urlsafe_base64
  end
end

class Post < ActiveRecord::Base
  belongs_to :user
  has_many :ratings
  has_many :feedbacks, as: :feedbackable

  validates_presence_of :title
  validates_presence_of :content
end

class Rating < ActiveRecord::Base
  belongs_to :post, counter_cache: true
  validates_inclusion_of :value, :in => 1..5, message: "Rating should be in between 1 to 5"
end

class Feedback < ActiveRecord::Base
  belongs_to :feedbackable, polymorphic: true
  belongs_to :owner, class_name: 'User'
end
