require 'rack'
require 'rack/handler/puma'
require 'pry'
require 'uri'
require 'nokogiri'
require_relative 'db_connection'
require_relative 'models'

def create_response(status = 200, res = {})
  @response = Rack::Response.new
  @response.content_type = "application/json; charset=UTF-8"
  @response.status = status
  @response.write(res.to_json)
  @response.close
  @response.finish
end

def create_post(user, params, auth_ip)
  post = user.posts.new(title: params['title'], content: params['content'], author_ip: auth_ip)
  if post.save
    res = { post: post.as_json, auth_token: user.login.auth_token }
    create_response(200, res)
  else
    res = { errors: post.errors.full_messages.as_json, auth_token: user.login.auth_token }
    create_response(422, res)
  end
end

def create_feedback(params, owner)
  if params['user_id'].present?
    user_feedback(params['user_id'].to_i, params['comment'], owner)
  elsif params['post_id'].present?
    post_feedback(params['post_id'].to_i, params['comment'], owner)
  else
    create_response(422, { errors: "Either user_id or post_id is required for feedback" })
  end
end

def post_feedback(post_id, comment, owner)
  post = Post.find_by(id: post_id)
  if post.present?
    post.feedbacks.create(comment: comment, owner: owner)
    feedbacks = Feedback.where(owner_id: owner.id).order(created_at: :desc)
    res = { feedbacks: feedbacks.as_json, login: { username: owner.username, auth_token: owner.login.auth_token } }
    create_response(200, res)
  else
    create_response(422, { errors: "No post with post_id: #{post_id} is present" })
  end
end

def user_feedback(user_id, comment, owner)
  user = User.find_by(id: user_id)
  if user.present?
    user.feedbacks.create(comment: comment, owner: owner)
    feedbacks = Feedback.where(owner_id: owner.id).order(created_at: :desc)
    res = { feedbacks: feedbacks.as_json, login: { username: owner.username, auth_token: owner.login.auth_token } }
    create_response(200, res)
  else
    create_response(422, { errors: "No user with user_id: #{user_id} is present" })
  end
end

app = -> environment {
  request = Rack::Request.new(environment)
  begin

    if request.post? && request.path == "/posts"
      params = JSON.parse(request.body.string)
      auth_token = request.env['HTTP_AUTH_TOKEN']
      auth_username = request.env['HTTP_USER_NAME']
      auth_ip = request.env['HTTP_IP']

      user = User.find_by(username: auth_username)
      if user.present?
        if user.login.auth_token != auth_token
          create_response(422, { errors: "Authentication Error", login: { username: user.username, auth_token: user.login.auth_token,
                                                                          message: "You won't do this in real life scenario" } })
        else
          create_post(user, params, auth_ip)
        end
      else
        user = User.create(name: auth_username, username: auth_username)
        user.create_login
        create_post(user, params, auth_ip)
      end

    elsif request.post? && request.path == "/post/ratings"
      params = JSON.parse(request.body.string)

      post = Post.find_by(id: params['post_id'])
      rating = post.ratings.new(value: params['value'])

      if rating.save
        post.average_rating = (post.average_rating + rating.value) / post.ratings_count
        post.save
        res = { 'average_rating': post.average_rating }
        create_response(200, res)
      else
        res = { errors: rating.errors.full_messages.as_json }
        create_response(422, res)
      end

    elsif request.get? && request.path == '/top_posts'
      uri = URI(request.url)
      query_params = Rack::Utils.parse_nested_query(uri.query)
      n = query_params['n'].to_i
      posts = Post.order(average_rating: :desc).limit(n).as_json(only: [:id, :title, :content])
      create_response(200, posts)

    elsif request.get? && request.path == '/ips'
      posts = Post.joins(:user).pluck("author_ip", "username")
      ips = posts.group_by { |i| i.shift }.map { |k, v| [k, v.flatten.uniq] }.to_h
      create_response(200, ips)

    elsif request.post? && request.path == '/feedbacks'
      params = JSON.parse(request.body.string)
      auth_token = request.env['HTTP_AUTH_TOKEN']
      auth_username = request.env['HTTP_USER_NAME'] || "user_#{Time.current.to_i}"
      owner_id = request.env['HTTP_OWNER_ID'].to_i
      owner = User.find_by(id: owner_id)

      if owner.present?
        if owner.login.auth_token != auth_token
          create_response(422, { errors: "Authentication Error", login: { username: owner.username, auth_token: owner.login.auth_token,
                                                                          message: "You won't do this in real life scenario" } })
        else
          create_feedback(params, owner)
        end
      else
        owner = User.create(name: auth_username, username: auth_username)
        owner.create_login
        create_feedback(params, owner)
      end

    else
      create_response(404, { errors: "Endpoint not found" })
    end
  rescue Exception => e
    create_response(422, { errors: e.message })
  end
}

Rack::Handler::Puma.run(app, :Port => 1337, :Verbose => true)



