require "faraday"
require "json"

class ApiClient
  def self.ips
    url = "http://0.0.0.0:1337/ips"
    response = Faraday.get(url)
    data = response.body
    status = response.status
    [JSON.parse(data, symbolize_names: true), { status: status }]
  end

  def self.top_posts(n)
    url = "http://0.0.0.0:1337/top_posts?n=#{n}"
    response = Faraday.get(url)
    data = response.body
    status = response.status
    [JSON.parse(data, symbolize_names: true), { status: status }]
  end

  def self.posts(username, auth_token, title, content, ip)

    url = "http://0.0.0.0:1337/posts"
    response = Faraday.post(url) do |req|
      req.headers['Content-Type'] = 'application/json'
      req.headers['ip'] = ip
      req.headers['auth_token'] = auth_token
      req.headers['user_name'] = username
      req.body = { title: title, content: content }.to_json
    end
    data = response.body
    status = response.status
    [JSON.parse(data, symbolize_names: true), { status: status }]
  end

  def self.post_ratings(post_id, value)
    url = "http://0.0.0.0:1337/post/ratings"
    response = Faraday.post(url) do |req|
      req.headers['Content-Type'] = 'application/json'
      req.body = { post_id: post_id, value: value }.to_json
    end
    data = response.body
    status = response.status
    [JSON.parse(data, symbolize_names: true), { status: status }]
  end

  def self.feedback(owner_id, username, auth_token, comment, post_id, user_id)
    url = "http://0.0.0.0:1337/feedbacks"
    response = Faraday.post(url) do |req|
      req.headers['Content-Type'] = 'application/json'
      req.headers['auth_token'] = auth_token
      req.headers['user_name'] = username
      req.headers['owner_id'] = owner_id
      req.body = { post_id: post_id, user_id: user_id, comment: comment }.to_json
    end
    data = response.body
    status = response.status
    [JSON.parse(data, symbolize_names: true), { status: status }]
  end
end
