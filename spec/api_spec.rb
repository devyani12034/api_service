require 'rspec'
require_relative '../models'
require_relative '../db_connection'

require_relative 'api_client'
describe ApiClient do
  describe '/ips' do
    let(:ips_response) { ApiClient.ips }
    it "returns correctly ips data" do
      expect(ips_response[0]).to be_kind_of(Hash)
      expect(ips_response[1][:status]).to eq(200)
      expect(ips_response[0].values.each { |v| v }).to be_kind_of(Array)
    end
  end

  describe '/top_posts' do
    let(:n) { Random.new.rand(1..50) }
    let(:post_response) { ApiClient.top_posts(n) }
    it "returns correctly posts data" do
      expect(post_response[0]).to be_kind_of(Array)
      expect(post_response[1][:status]).to eq(200)
      expect(post_response[0].size).to eq(n)
      expect(post_response[0][0]).to be_kind_of(Hash)
      expect(post_response[0][0]).to have_key(:id)
      expect(post_response[0][0]).to have_key(:title)
      expect(post_response[0][0]).to have_key(:content)
      expect(post_response[0][0][:id]).to be_kind_of(Integer)
      expect(post_response[0][0][:title]).to be_kind_of(String)
      expect(post_response[0][0][:content]).to be_kind_of(String)
    end
  end

  describe '/posts' do
    let(:user) { User.first }
    let(:title) { "Title of post" }
    let(:ip) { "127.0.0.1" }
    let(:content) { "Content of post" }
    let(:post_response) { ApiClient.posts(user.username, user.login.auth_token, title, content, ip) }
    it "returns correctly posts data" do
      expect(post_response[0]).to be_kind_of(Hash)
      expect(post_response[1][:status]).to eq(200)
      # expect(post_response[0].size).to eq(n)
      # expect(post_response[0][0]).to be_kind_of(Hash)
      expect(post_response[0]).to have_key(:post)
      expect(post_response[0]).to have_key(:auth_token)
      expect(post_response[0][:post]).to have_key(:title)
      expect(post_response[0][:post]).to have_key(:content)
      expect(post_response[0][:post]).to have_key(:user_id)
      expect(post_response[0][:post]).to have_key(:author_ip)
      expect(post_response[0][:post][:title]).to eq(title)
      expect(post_response[0][:post][:content]).to eq(content)
      expect(post_response[0][:post][:user_id]).to eq(user.id)
      expect(post_response[0][:post][:author_ip]).to eq(ip)
      expect(post_response[0][:auth_token]).to eq(user.login.auth_token)
    end
  end

  describe '/posts auth error' do
    let(:username) { "Dev" }
    let(:title) { "Title of post" }
    let(:ip) { "127.0.0.1" }
    let(:content) { "Content of post" }
    let(:post_response) { ApiClient.posts(username, "", title, content, ip) }
    it "returns correctly posts data" do
      expect(post_response[0]).to be_kind_of(Hash)
      expect(post_response[1][:status]).to eq(422)
      expect(post_response[0]).to have_key(:errors)
      expect(post_response[0]).to have_key(:login)
      expect(post_response[0][:login]).to have_key(:username)
      expect(post_response[0][:login]).to have_key(:auth_token)
      expect(post_response[0][:login]).to have_key(:message)
      expect(post_response[0][:login][:username]).to eq(username)
      expect(post_response[0][:login][:auth_token]).to be_kind_of(String)
    end
  end

  describe '/posts auth error' do
    let(:username) { "Dev" }
    let(:title) { "Title of post" }
    let(:ip) { "127.0.0.1" }
    let(:content) { "Content of post" }
    let(:post_response) { ApiClient.posts(username, "", title, content, ip) }
    it "returns correctly posts data" do
      expect(post_response[0]).to be_kind_of(Hash)
      expect(post_response[1][:status]).to eq(422)
      expect(post_response[0]).to have_key(:errors)
      expect(post_response[0]).to have_key(:login)
      expect(post_response[0][:login]).to have_key(:username)
      expect(post_response[0][:login]).to have_key(:auth_token)
      expect(post_response[0][:login]).to have_key(:message)
      expect(post_response[0][:login][:username]).to eq(username)
      expect(post_response[0][:login][:auth_token]).to be_kind_of(String)
    end
  end

  describe '/post/ratings' do
    let(:post) { Post.first }
    let(:value) { 4 }
    let(:response) { ApiClient.post_ratings(post.id, value) }
    it "returns correctly posts data" do
      expect(response[0]).to be_kind_of(Hash)
      expect(response[1][:status]).to eq(200)
      expect(response[0]).to have_key(:average_rating)
      expect(response[0][:average_rating]).to be_kind_of(String)
    end
  end

  describe '/post/ratings Value Error' do
    let(:post) { Post.first }
    let(:value) { 7 }
    let(:response) { ApiClient.post_ratings(post.id, value) }
    it "returns correctly posts data" do
      expect(response[0]).to be_kind_of(Hash)
      expect(response[1][:status]).to eq(422)
      expect(response[0]).to have_key(:errors)
      expect(response[0][:errors]).to be_kind_of(Array)
      expect(response[0][:errors][0]).to eq("Value Rating should be in between 1 to 5")
    end
  end

  describe '/feedbacks post' do
    let(:post) { Post.first }
    let(:owner) { User.first }
    let(:comment) { "Feedback comment" }
    let(:response) { ApiClient.feedback(owner.id.to_s, owner.username, owner.login.auth_token, comment, post.id, nil) }
    it "returns correctly posts data" do
      expect(response[0]).to be_kind_of(Hash)
      expect(response[1][:status]).to eq(200)
      expect(response[0]).to have_key(:feedbacks)
      expect(response[0]).to have_key(:login)
      expect(response[0][:feedbacks]).to be_kind_of(Array)
      expect(response[0][:feedbacks][0]).to be_kind_of(Hash)
      expect(response[0][:feedbacks][0]).to have_key(:owner_id)
      expect(response[0][:feedbacks][0]).to have_key(:comment)
      expect(response[0][:feedbacks][0]).to have_key(:feedbackable_type)
      expect(response[0][:feedbacks][0]).to have_key(:feedbackable_id)
      expect(response[0][:feedbacks][0][:owner_id]).to eq(owner.id)
      expect(response[0][:feedbacks][0][:comment]).to eq(comment)
      expect(response[0][:feedbacks][0][:feedbackable_type]).to eq("Post")
      expect(response[0][:feedbacks][0][:feedbackable_id]).to eq(post.id)
    end
  end

  describe '/feedbacks user' do
    let(:user) { User.last }
    let(:owner) { User.first }
    let(:comment) { "Feedback comment" }
    let(:response) { ApiClient.feedback(owner.id.to_s, owner.username, owner.login.auth_token, comment, nil, user.id) }
    it "returns correctly posts data" do
      expect(response[0]).to be_kind_of(Hash)
      expect(response[1][:status]).to eq(200)
      expect(response[0]).to have_key(:feedbacks)
      expect(response[0]).to have_key(:login)
      expect(response[0][:feedbacks]).to be_kind_of(Array)
      expect(response[0][:feedbacks][0]).to be_kind_of(Hash)
      expect(response[0][:feedbacks][0]).to have_key(:owner_id)
      expect(response[0][:feedbacks][0]).to have_key(:comment)
      expect(response[0][:feedbacks][0]).to have_key(:feedbackable_type)
      expect(response[0][:feedbacks][0]).to have_key(:feedbackable_id)
      expect(response[0][:feedbacks][0][:owner_id]).to eq(owner.id)
      expect(response[0][:feedbacks][0][:comment]).to eq(comment)
      expect(response[0][:feedbacks][0][:feedbackable_type]).to eq("User")
      expect(response[0][:feedbacks][0][:feedbackable_id]).to eq(user.id)
    end
  end

  describe '/feedbacks user' do
    let(:user) { User.last }
    let(:owner) { User.first }
    let(:comment) { "Feedback comment" }
    let(:response) { ApiClient.feedback(owner.id.to_s, owner.username, owner.login.auth_token, comment, nil, user.id) }
    it "returns correctly posts data" do
      expect(response[0]).to be_kind_of(Hash)
      expect(response[1][:status]).to eq(200)
      expect(response[0]).to have_key(:feedbacks)
      expect(response[0]).to have_key(:login)
      expect(response[0][:feedbacks]).to be_kind_of(Array)
      expect(response[0][:feedbacks][0]).to be_kind_of(Hash)
      expect(response[0][:feedbacks][0]).to have_key(:owner_id)
      expect(response[0][:feedbacks][0]).to have_key(:comment)
      expect(response[0][:feedbacks][0]).to have_key(:feedbackable_type)
      expect(response[0][:feedbacks][0]).to have_key(:feedbackable_id)
      expect(response[0][:feedbacks][0][:owner_id]).to eq(owner.id)
      expect(response[0][:feedbacks][0][:comment]).to eq(comment)
      expect(response[0][:feedbacks][0][:feedbackable_type]).to eq("User")
      expect(response[0][:feedbacks][0][:feedbackable_id]).to eq(user.id)
    end
  end

  describe '/feedbacks error' do
    let(:user) { User.last }
    let(:owner) { User.first }
    let(:comment) { "Feedback comment" }
    let(:response) { ApiClient.feedback(owner.id.to_s, owner.username, "random", comment, nil, user.id) }
    it "returns correctly posts data" do
      expect(response[0]).to be_kind_of(Hash)
      expect(response[1][:status]).to eq(422)
      expect(response[0]).to have_key(:errors)
      expect(response[0]).to have_key(:login)
      expect(response[0][:login][:username]).to eq(owner.username)
      expect(response[0][:login][:auth_token]).to eq(owner.login.auth_token)
    end
  end
end
