require 'sidekiq'
require 'nokogiri'
require_relative 'models'
require_relative 'db_connection'

Sidekiq.configure_client do |config|
  config.redis = { db: 1 }
end

Sidekiq.configure_server do |config|
  config.redis = { db: 1 }
end

class FeedbackWorker
  include Sidekiq::Worker

  def perform
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.root {
        xml.feedbacks {
          Feedback.all.each do |feedback|
            xml.feedback {
              xml.id_ feedback.id
              xml.owner_ feedback.owner.username
              xml.comment_ feedback.comment
              xml.rating_ feedback.feedbackable_type == "Post" ? feedback.feedbackable.average_rating : ""
              xml.type_ feedback.feedbackable_type
            }
          end
        }
      }
    end
    File.open("feedback_#{Time.current.to_i}.xml", 'w') { |file| file.write(builder.to_xml) }
  end
end